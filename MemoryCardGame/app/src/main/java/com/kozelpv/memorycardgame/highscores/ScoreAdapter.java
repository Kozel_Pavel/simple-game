package com.kozelpv.memorycardgame.highscores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozelpv.memorycardgame.R;

import java.util.List;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ScoreViewHolder> {

    private final LayoutInflater mInflater;
    private List<Score> highscoreList;

    public ScoreAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ScoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_highscore, parent, false);
        return new ScoreViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreViewHolder holder, int position) {
        holder.onBind(position);
    }

    public void updateHigescoresList(List<Score> highscoreList) {
        this.highscoreList = highscoreList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return highscoreList != null ? highscoreList.size() : 0;
    }

    class ScoreViewHolder extends RecyclerView.ViewHolder {
        private TextView number;
        private TextView nickname;
        private TextView highscore;

        private ScoreViewHolder(@NonNull View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.positionNumber);
            nickname = itemView.findViewById(R.id.nickname);
            highscore = itemView.findViewById(R.id.highscore);
        }

        private void onBind(int position) {
            if (highscoreList != null) {
                final Score score = highscoreList.get(position);
                number.setText((position + 1) + ")");
                nickname.setText(score.getNickname());
                highscore.setText(String.valueOf(score.getHighscore()));
            } else {
                nickname.setText(R.string.no_highscore);
            }
        }
    }
}
