package com.kozelpv.memorycardgame.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.kozelpv.memorycardgame.database.ScoreRoomDatabase;
import com.kozelpv.memorycardgame.highscores.Score;
import com.kozelpv.memorycardgame.highscores.ScoreDao;

import java.util.List;

public class HighscoreRepository {

    private ScoreDao scoreDao;
    private LiveData<List<Score>> highscoresList;
    private static HighscoreRepository thisRepository;

    private HighscoreRepository(Application application) {
        ScoreRoomDatabase db = ScoreRoomDatabase.getDatabase(application);
        this.scoreDao = db.getScoreDao();
    }

    public static HighscoreRepository getRepositiryInstance(Application application) {
        if (thisRepository == null) {
            thisRepository = new HighscoreRepository(application);
        }
        return thisRepository;
    }

    public LiveData<List<Score>> getHighscoresList() {
        if (highscoresList == null) {
            highscoresList = scoreDao.getAllHighscores();
        }
        return highscoresList;
    }

    public void insert(final Score score) {
        ScoreRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                scoreDao.insert(score);
            }
        });
    }

    public void deleteHighscoreById(final int highscoreId) {
        ScoreRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                scoreDao.deleteHighscoreById(highscoreId);
            }
        });
    }
}
