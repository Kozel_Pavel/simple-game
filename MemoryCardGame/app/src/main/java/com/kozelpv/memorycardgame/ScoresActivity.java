package com.kozelpv.memorycardgame;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kozelpv.memorycardgame.highscores.HigscoreViewModel;
import com.kozelpv.memorycardgame.highscores.Score;
import com.kozelpv.memorycardgame.highscores.ScoreAdapter;
import com.kozelpv.memorycardgame.repository.HighscoreRepository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScoresActivity extends AppCompatActivity {

    @BindView(R.id.scoreTable)
    RecyclerView scoreTable;

    private ScoreAdapter scoreAdapter;
    private HigscoreViewModel higscoreViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscores_list);
        ButterKnife.bind(this);
        recyclerViewInit();
        scoreViewModelInit();
    }

    private void recyclerViewInit() {
        scoreAdapter = new ScoreAdapter(this);
        scoreTable.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        scoreTable.setAdapter(scoreAdapter);
    }

    private void scoreViewModelInit() {
        higscoreViewModel = ViewModelProviders.of(this).get(HigscoreViewModel.class);
        higscoreViewModel.getHighscoreList().observe(this, new Observer<List<Score>>() {
            @Override
            public void onChanged(List<Score> highscoreList) {
                scoreAdapter.updateHigescoresList(highscoreList);
            }
        });
    }
}
