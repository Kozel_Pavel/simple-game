package com.kozelpv.memorycardgame.highscores;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.kozelpv.memorycardgame.repository.HighscoreRepository;

import java.util.List;

public class HigscoreViewModel extends AndroidViewModel {
    private HighscoreRepository highscoreRepository;
    private LiveData<List<Score>> highscoreList;

    public HigscoreViewModel(@NonNull Application application) {
        super(application);
        highscoreRepository = HighscoreRepository.getRepositiryInstance(application);
        highscoreList = highscoreRepository.getHighscoresList();
    }

    public LiveData<List<Score>> getHighscoreList() {
        return highscoreList;
    }

    public void insert(Score highscore) {
        highscoreRepository.insert(highscore);
    }

    public void deleteById(int highscoreId) {
        highscoreRepository.deleteHighscoreById(highscoreId);
    }
}
