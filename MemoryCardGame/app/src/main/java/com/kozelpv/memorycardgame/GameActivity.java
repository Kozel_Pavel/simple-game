package com.kozelpv.memorycardgame;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.kozelpv.memorycardgame.highscores.Score;
import com.kozelpv.memorycardgame.repository.HighscoreRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.kozelpv.memorycardgame.R.drawable.*;

public class GameActivity extends AppCompatActivity implements AddHighscoreDialogFragment.AddHighscoreDialogListener {

    @BindView(R.id.scorePoint)
    TextView scorePoint;

    @BindViews({R.id.card_1_1, R.id.card_1_2, R.id.card_1_3, R.id.card_1_4,
            R.id.card_2_1, R.id.card_2_2, R.id.card_2_3, R.id.card_2_4,
            R.id.card_3_1, R.id.card_3_2, R.id.card_3_3, R.id.card_3_4,
            R.id.card_4_1, R.id.card_4_2, R.id.card_4_3, R.id.card_4_4})
    List<Button> buttonList;

    private ArrayList<Integer> heroesList = new ArrayList<>(Arrays.asList(
            antman, captain_america, deadpool, groot,
            hulk, spiderman, thor, winter_soldier,
            antman, captain_america, deadpool, groot,
            hulk, spiderman, thor, winter_soldier
    ));

    private int firstClickCardIndex = -1, secondClickCardIndex = -1;
    private int cardNumber = 1;

    private int pareLeft = 8;
    private int turn = 0;

    private HighscoreRepository highscoreRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        Collections.shuffle(heroesList);
        for (int index = 0; index < buttonList.size(); index++) {
            Button button = buttonList.get(index);
            button.setText(String.valueOf(question_hero));
            button.setTextSize(0f);
            final int finalIndex = index;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Button button = (Button) view;
                    if (cardNumber == 1) {
                        openCard(button, heroesList.get(finalIndex));
                        firstClickCardIndex = finalIndex;
                        button.setClickable(false);
                        cardNumber++;
                    } else if (cardNumber == 2 && button.getText() != String.valueOf(question_hero)) {
                        openCard(button, heroesList.get(finalIndex));
                        secondClickCardIndex = finalIndex;
                        checkMatch(firstClickCardIndex, secondClickCardIndex);
                        cardNumber = 1;
                        turn++;
                        scorePoint.setText(String.valueOf(turn));
                    }
                }
            });

        }
    }

    private void openCard(Button button, int cardImage) {
        button.setBackgroundResource(cardImage);
        button.setText(String.valueOf(cardImage));
    }

    private void closeOpenCards(int firstClickCardIndex, int secondClickCardIndex) {
        closeCard(buttonList.get(firstClickCardIndex));
        closeCard(buttonList.get(secondClickCardIndex));
    }

    private void closeCard(Button button) {
        button.setBackgroundResource(question_hero);
        button.setText(String.valueOf(question_hero));
    }

    private void checkMatch(int firstClickCardIndex, int secondClickCardIndex) {
        String firstCard = (String) buttonList.get(firstClickCardIndex).getText();
        String secondCard = (String) buttonList.get(secondClickCardIndex).getText();
        if (firstCard.equals(secondCard)) {
            Toast.makeText(this, "Good, job!", Toast.LENGTH_SHORT).show();
            buttonList.get(secondClickCardIndex).setClickable(false);
            pareLeft--;
            if (pareLeft <= 0) {
                pareLeft = 8;
                showSaveHighscoreDialog();
            }
        } else {
            Toast.makeText(this, "Try again", Toast.LENGTH_SHORT).show();
            SimpleDelay simpleDelay = new SimpleDelay();
            simpleDelay.execute(1 * 1000L);
        }
    }

    private void showSaveHighscoreDialog() {
        DialogFragment dialog = new AddHighscoreDialogFragment();
        dialog.show(this.getSupportFragmentManager(), AddHighscoreDialogFragment.FRAGMENT_TAG);
    }

    @Override
    public void onComplite(String nickname) {
        highscoreRepository = HighscoreRepository.getRepositiryInstance(getApplication());
        highscoreRepository.insert(new Score(nickname, turn));
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private class SimpleDelay extends AsyncTask<Long, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            for (int index = 0; index < buttonList.size(); index++) {
                buttonList.get(index).setClickable(false);
            }
        }

        @Override
        protected Void doInBackground(Long... longs) {
            try {
                TimeUnit.MILLISECONDS.sleep(longs[0]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            closeOpenCards(firstClickCardIndex, secondClickCardIndex);
            for (int index = 0; index < buttonList.size(); index++) {
                Button button = buttonList.get(index);
                if (button.getText().equals(String.valueOf(question_hero))) {
                    buttonList.get(index).setClickable(true);
                }
            }
        }
    }
}
