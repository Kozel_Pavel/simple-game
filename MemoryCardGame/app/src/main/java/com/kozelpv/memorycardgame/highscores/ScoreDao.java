package com.kozelpv.memorycardgame.highscores;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ScoreDao {
    @Insert
    void insert(Score score);

    @Query("SELECT * FROM highscore WHERE highscore_id = :highscoreId")
    Score getHighscoreById(int highscoreId);

    //Get all highscore from DB order by score
    @Query("SELECT * FROM highscore ORDER BY highscore ASC ")
    LiveData<List<Score>> getAllHighscores();

    //Delete score by Id
    @Query("DELETE FROM highscore WHERE highscore_id = :highscoreId")
    void deleteHighscoreById(int highscoreId);

    //Delete all highscore from DB
    @Query("DELETE FROM highscore")
    void deleteAllHighscores();
}
