package com.kozelpv.memorycardgame.highscores;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "highscore")
public class Score {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "highscore_id")
    private int highscoreId;

    @ColumnInfo(name = "nickname")
    private String nickname;

    @ColumnInfo(name = "highscore")
    private int highscore;

    public Score(String nickname, int highscore) {
        this.nickname = nickname;
        this.highscore = highscore;
    }

    public void setHighscoreId(int highscoreId) {
        this.highscoreId = highscoreId;
    }

    public int getHighscoreId() {
        return highscoreId;
    }

    public String getNickname() {
        return nickname;
    }

    public int getHighscore() {
        return highscore;
    }

    @NonNull
    @Override
    public String toString() {
        return "Score{" +
                "id=" + highscoreId + "\'" +
                "Nickname=" + nickname + "\'" +
                "Highscore=" + highscore +
                "}";
    }
}
