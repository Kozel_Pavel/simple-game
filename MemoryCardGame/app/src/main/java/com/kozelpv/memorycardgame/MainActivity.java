package com.kozelpv.memorycardgame;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button startButton,
            scopeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(this);
        scopeButton = findViewById(R.id.scopeButton);
        scopeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startButton:
                Intent intent1 = new Intent(this, GameActivity.class);
                startActivity(intent1);
                break;
            case R.id.scopeButton:
                Intent intent2 = new Intent(this, ScoresActivity.class);
                startActivity(intent2);
                break;
        }
    }
}
