package com.kozelpv.memorycardgame;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class AddHighscoreDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = AddHighscoreDialogFragment.class.getName();
    public static final String TAG_NICKNAME = "YOUR_NICKNAME";

    private View layout;
    private EditText editNickname;

    public interface AddHighscoreDialogListener {
        void onComplite(String nickname);
    }

    private AddHighscoreDialogListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (AddHighscoreDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement AddHighscoreDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        layout = inflater.inflate(R.layout.dialog_view, null);
        editNickname = layout.findViewById(R.id.editNickname);

        builder.setView(layout)
                .setTitle(R.string.editText_help_name)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String cityName = String.valueOf(editNickname.getText());
                        listener.onComplite(cityName);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onComplite("Empty");
                    }
                });
        return builder.create();
    }

    @Override
    public void onDestroyView() {
        listener = null;
        super.onDestroyView();
    }
}
